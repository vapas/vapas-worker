create table vapas_featured
(
	url text not null,
	title text not null,
	package text PRIMARY KEY,
	hide_shadow bool not null
)