-- Your SQL goes here
create table vapas_payment_info
(
    name text not null PRIMARY KEY,
    description text not null,
    banner_message text not null
);